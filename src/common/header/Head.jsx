import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

const Head = () => {
    return (
        <>
            <Container
                fluid
                className="justify-content-center text-center  text-light"
                style={{ background: '#001c61' }}
            >
                <Row>
                    <Col md={6} lg={6}>
                        <label className="m-2 mx-4">+63123456789</label>
                        <label className="m-2 mx-4">zuitt_store@gmail.com</label>
                    </Col>
                    <Col md={6} lg={6}>
                        <label className="m-2 mx-4">FAQ"s</label>
                        <label className="m-2 mx-4">Need Help?</label>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Head;
