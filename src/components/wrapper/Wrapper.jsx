import React from 'react';
import { Card } from 'react-bootstrap';
import './style.css';

const Wrapper = () => {
    const data = [
        {
            cover: <i class="fa-solid fa-truck-fast"></i>,
            title: 'Worldwide Delivery',
            decs: 'We offer competitive prices on our 100 million plus product any range.',
        },
        {
            cover: <i class="fa-solid fa-id-card"></i>,
            title: 'Safe Payment',
            decs: 'We offer competitive prices on our 100 million plus product any range.',
        },
        {
            cover: <i class="fa-solid fa-shield"></i>,
            title: 'Shop With Confidence ',
            decs: 'We offer competitive prices on our 100 million plus product any range.',
        },
        {
            cover: <i class="fa-solid fa-headset"></i>,
            title: '24/7 Support ',
            decs: 'We offer competitive prices on our 100 million plus product any range.',
        },
    ];
    return (
        <>
            <div className="container text-center ">
                <div className="row">
                    {data.map((val, index) => {
                        return (
                            <div className="col-md-3 my-5" key={index}>
                                <div className="card h-auto shadow border-0">
                                    <div
                                        className="card-img-top icon-circle"
                                        style={{ fontSize: '2rem' }}
                                    >
                                        <i>{val.cover}</i>
                                    </div>
                                    <div className="card-body">
                                        <h5 className="card-title">{val.title}</h5>
                                        <p className="card-text">{val.decs}</p>
                                    </div>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );
};

export default Wrapper;
