import React from 'react';
import Categories from './Categories';
// import './Home.css';
import SliderHome from './Slider';
import { Container, Row, Col } from 'react-bootstrap';

const Home = () => {
    return (
        <>
            <Container>
                <Row>
                    <Col
                        md={3}
                        lg={3}
                        className="mt-3 "
                        style={{ boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)' }}
                    >
                        <Categories />
                    </Col>
                    <Col md={9} lg={9} className="mt-5">
                        <SliderHome />
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Home;
