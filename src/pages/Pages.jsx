import React from 'react';
import Home from '../components/MainPage/Home';
import NewArrivals from '../components/newarrivals/NewArrivals';

import Shop from '../components/shops/Shop';
import Annocument from '../components/annocument/Annocument';
import Wrapper from '../components/wrapper/Wrapper';
import Footer from '../common/footer/Footer';

const Pages = ({ productItems, addToCart, CartItem, shopItems }) => {
    return (
        <>
            <Home className="my-5" CartItem={CartItem} />
            <NewArrivals className="my-5" />
            <Shop className="my-5" shopItems={shopItems} addToCart={addToCart} />
            <Annocument className="my-5" />
            <Wrapper className="my-5" />
            <Footer className="my-5" />
        </>
    );
};

export default Pages;
