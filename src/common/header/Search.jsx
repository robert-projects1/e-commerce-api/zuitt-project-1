import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

const Search = ({ CartItem }) => {
    window.addEventListener('scroll', function () {
        const search = document.querySelector('.search');
        search.classList.toggle('active', window.scrollY > 100);
    });

    return (
        <>
            <Container fluid className="bg-light p-3 justify-content-center text-center">
                <Row className="align-items-center">
                    <Col md={2} lg={2} className="my-sm-2">
                        <div>
                            <img
                                src="https://d3ojsfl21hfxhc.cloudfront.net/assets/zuitt/zuittlogo.png"
                                alt=""
                            />
                        </div>
                    </Col>
                    <Col md={8} lg={8} className="my-sm-2">
                        <Container fluid className="justify-content-center text-center">
                            <Row className="align-items-center">
                                <Col md={10} lg={10} className="d-flex align-items-center">
                                    <input
                                        className="mx-3 rounded-5 border-0 w-100 "
                                        type="text"
                                        placeholder="Search here "
                                        style={{
                                            padding: '20px',
                                        }}
                                    />
                                </Col>
                                <Col md={2} lg={2} className="my-sm-2">
                                    <span className="">All Products</span>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                    <Col md={2} lg={2} sm={12} className="my-sm-2">
                        <Container fluid className=" ">
                            <Row className="">
                                <Col
                                    sm={12}
                                    md={12}
                                    lg={12}
                                    className="d-flex justify-content-center text-center"
                                >
                                    {/* <i className="fa fa-user icon-circle"></i> */}
                                    <i
                                        className="fa fa-user icon-circle mx-2"
                                        style={{
                                            borderRadius: '50%',
                                            backgroundColor: ' #cacaca ',
                                            padding: '18px',
                                            color: ' #1c1c1c ',
                                        }}
                                    ></i>
                                    <div className="cart">
                                        <Link to="/cart">
                                            {/* <i className="fa fa-shopping-bag icon-circle"></i> */}
                                            <i
                                                className="fa fa-shopping-bag mx-2"
                                                style={{
                                                    borderRadius: '50%',
                                                    backgroundColor: '  #cacaca  ',
                                                    padding: '18px',
                                                    color: ' #0059ff ',
                                                }}
                                            ></i>
                                            <span>
                                                {CartItem.length === 0 ? '' : CartItem.length}
                                            </span>
                                        </Link>
                                    </div>
                                </Col>
                            </Row>
                        </Container>
                    </Col>
                </Row>
            </Container>
        </>
    );
};

export default Search;
