import React from 'react';
import Catg from './Catg';
import ShopCart from './ShopCart';
import './style.css';

const Shop = ({ addToCart, shopItems }) => {
    return (
        <>
            {/* <section className='shop background'>
        <div className='container d_flex'>
          <Catg />

          <div className='contentWidth'>
            <div className='heading d_flex'>
              <div className='heading-left row  f_flex'>
                <h2>Mobile Phones</h2>
              </div>
              <div className='heading-right row '>
                <span>View all</span>
                <i className='fa-solid fa-caret-right'></i>
              </div>
            </div>
            <div className='product-content  grid1'>
              <ShopCart addToCart={addToCart} shopItems={shopItems} />
            </div>
          </div>
        </div>
      </section> */}
            <div className="container my-5">
                <div className="row">
                    <div className="col-md-3">
                        <Catg />
                    </div>
                    <div className="col-md-9">
                        <div className="contentWidth">
                            <div className="heading d-flex">
                                <div className="heading-left col-md-9  f-flex">
                                    <h2>Mobile Phones</h2>
                                </div>
                                <div className="heading-right col-md-3 ">
                                    <span>View all</span>
                                    <i className="fa-solid fa-caret-right"></i>
                                </div>
                            </div>
                            <div className="product-content  grid1">
                                <ShopCart addToCart={addToCart} shopItems={shopItems} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Shop;
