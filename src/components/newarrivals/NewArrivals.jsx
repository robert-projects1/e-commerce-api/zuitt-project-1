import React from 'react';
import Cart from './Cart';
import './style.css';

const NewArrivals = () => {
    return (
        <>
            <section className="NewArrivals background">
                <div className="container fluid my-4">
                    <div className="heading d_flex">
                        <div className="row d-flex align-items-center">
                            <div className="col-md-1 ">
                                <img
                                    style={{ width: '55px', height: '50px' }}
                                    src="https://d1rlzxa98cyc61.cloudfront.net/wysiwyg/pages/1-newicon-jan2023.png"
                                />
                            </div>
                            <div className="col-9">
                                <h2>New Arrivals</h2>
                            </div>
                            <div className="heading-right col-auto ">
                                <span className="me-auto">View all</span>
                                <i className="fa-solid fa-caret-right"></i>
                            </div>
                        </div>
                    </div>

                    <Cart />
                </div>
            </section>
        </>
    );
};

export default NewArrivals;
