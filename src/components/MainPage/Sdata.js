const Sdata = [
    {
        id: 1,
        title: '50% Off For Your First Shopping',
        desc: "Don't miss out on this amazing opportunity to save big and start your shopping journey with a bang. Shop now and take advantage of this limited time offer!",
        cover: './images/SlideCard/slide-1.png',
    },
    {
        id: 2,
        title: 'Your First Shopping Experience Just Got Better! Get a 50% Discount Now.',
        desc: 'This limited time deal is the perfect opportunity to treat yourself or your loved ones with amazing products at unbeatable prices. Visit us now and start your shopping journey with a bang!',
        cover: './images/SlideCard/slide-2.png',
    },
    {
        id: 3,
        title: 'Get Ready to Save! Enjoy 50% Off on Your First Shopping Experience.',
        desc: "From fashion to electronics, we have something for everyone. Don't miss out on this amazing opportunity to save big and start your shopping journey with a bang. Shop now and take advantage of this limited time offer!",
        cover: './images/SlideCard/slide-3.png',
    },
    {
        id: 4,
        title: "Don't Miss Out on Our Special Offer! Get 50% Off on Your First Purchase Today.",
        desc: "Don't miss out on this incredible offer and start exploring our wide range of products. Whether you're looking for a new wardrobe or the latest gadgets, we've got you covered. Shop now and save big!",
        cover: './images/SlideCard/slide-4.png',
    },
];
export default Sdata;
