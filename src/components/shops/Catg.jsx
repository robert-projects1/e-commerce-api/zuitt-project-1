import React from 'react';

const Catg = () => {
    const data = [
        {
            cateImg: './images/category/cat-1.png',
            cateName: 'Apple',
        },
        {
            cateImg: './images/category/cat-2.png',
            cateName: 'Samasung',
        },
        {
            cateImg: './images/category/cat-1.png',
            cateName: 'Oppo',
        },
        {
            cateImg: './images/category/cat-2.png',
            cateName: 'Vivo',
        },
        {
            cateImg: './images/category/cat-1.png',
            cateName: 'Redimi',
        },
        {
            cateImg: './images/category/cat-2.png',
            cateName: 'Sony',
        },
    ];
    return (
        <>
            <div className="category container shadow p-4 rounded-2">
                <div className="chead d_flex row">
                    <h1 className="col">Brands </h1>
                    <h1 className="col">Shops </h1>
                </div>
                <div className="row">
                    <div className="col">
                        {data.map((value, index) => {
                            return (
                                <div className="container bg-light my-4" key={index}>
                                    <img className=" p-3" src={value.cateImg} alt="" />
                                    <span>{value.cateName}</span>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Catg;
