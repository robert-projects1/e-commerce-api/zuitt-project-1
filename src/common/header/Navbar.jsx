import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Navbar, Nav, Container } from 'react-bootstrap';

const CustomNavbar = () => {
    const [expanded, setExpanded] = useState(false);

    return (
        <Navbar
            bg="light"
            expand="lg"
            expanded={expanded}
            style={{ boxShadow: '0px 2px 5px rgba(0, 0, 0, 0.3)' }}
        >
            <Container>
                <Navbar.Brand href="#">
                    <div className="align-items-center d-flex">
                        <span
                            class="fa-solid fa-border-all mx-2"
                            style={{ fontSize: '30px' }}
                        ></span>
                        <h4 className="align-items-center">
                            Categories <i className="fa fa-chevron-down"></i>
                        </h4>
                    </div>
                </Navbar.Brand>
                <Navbar.Toggle onClick={() => setExpanded(expanded ? false : 'expanded')} />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="ms-auto">
                        <Link
                            to="/"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            Home
                        </Link>
                        <Link
                            to="/about"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            Pages
                        </Link>
                        <Link
                            to="/contact"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            User Account
                        </Link>
                        <Link
                            to="/about"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            Vendor account
                        </Link>
                        <Link
                            to="/about"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            Track My Order
                        </Link>
                        <Link
                            to="/about"
                            className="mx-3 text-decoration-none text-dark"
                            onClick={() => setExpanded(false)}
                        >
                            Contact
                        </Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
};

export default CustomNavbar;
