import React from 'react';
import Ndata from './Ndata';

const Cart = () => {
    return (
        <>
            {/* <div className="content grid product">
                {Ndata.map((val, index) => {
                    return (
                        <div className="box" key={index}>
                            <div className="img">
                                <img className="img-fluid" src={val.cover} alt="" />
                            </div>
                            <h4>{val.name}</h4>
                            <span>${val.price}</span>
                        </div>
                    );
                })}
            </div> */}
            {/* <div className="container-fluid">
                <div className="row">
                    <div className="col-md-2 col-10">
                        {Ndata.map((val, index) => {
                            return (
                                <div className="box" key={index}>
                                    <div className="img">
                                        <img className="img-fluid" src={val.cover} alt="" />
                                    </div>
                                    <h4>{val.name}</h4>
                                    <span>${val.price}</span>
                                </div>
                            );
                        })}
                    </div>
                </div>
            </div> */}
            <div className="container-fluid">
                <div className="row justify-content-center text-center ">
                    {Ndata.map((val, index) => {
                        return (
                            <div className="col-md-2 col-5" key={index}>
                                <div className="box">
                                    <div className="img ">
                                        <img className="img-fluid " src={val.cover} alt="" />
                                    </div>
                                    <h4 className="my-3">{val.name}</h4>
                                    <span>${val.price}</span>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        </>
    );
};

export default Cart;
