import React from 'react';
import './style.css';

const Footer = () => {
    return (
        <>
            <footer>
                <div className="container">
                    <div className="row">
                        <div className="col-3">
                            <img src="https://d3ojsfl21hfxhc.cloudfront.net/assets/zuitt/zuittlogo.png"></img>
                            <p>
                                Welcome to our online store! We offer a wide selection of
                                high-quality products at affordable prices. Our inventory includes
                                everything from clothing and accessories to home goods and
                                electronics. With fast shipping and excellent customer service, we
                                strive to make your shopping experience as seamless and enjoyable as
                                possible. Browse our collection today and find the perfect item to
                                suit your needs and style!
                            </p>
                            <div className="icon d_flex row text-center justify-content-center">
                                <div className="img d_flex col-4 m-1">
                                    <i class="fa-brands fa-google-play"></i>
                                    <span>Google Play</span>
                                </div>
                                <div className="img d_flex col-4 m-1">
                                    <i class="fa-brands fa-app-store-ios"></i>
                                    <span>App Store</span>
                                </div>
                            </div>
                        </div>

                        <div className="col-3">
                            <h2>About Us</h2>
                            <ul>
                                <li>Careers</li>
                                <li>Our Stores</li>
                                <li>Our Cares</li>
                                <li>Terms & Conditions</li>
                                <li>Privacy Policy</li>
                            </ul>
                        </div>

                        <div className="col-3">
                            <h2>Customer Care</h2>
                            <ul>
                                <li>Help Center </li>
                                <li>How to Buy </li>
                                <li>Track Your Order </li>
                                <li>Corporate & Bulk Purchasing </li>
                                <li>Returns & Refunds </li>
                            </ul>
                        </div>

                        <div className="col-3">
                            <h2>Contact Us</h2>
                            <ul>
                                <li>#340 St., Marcus Building, Makati City, Philippines</li>
                                <li>Email: zuitt.help@gmail.com</li>
                                <li>Phone: +63928333294</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </>
    );
};

export default Footer;
