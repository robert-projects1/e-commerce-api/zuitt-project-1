import React from 'react';
import Sdata from './Sdata';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';
import { Container, Row, Col, Button } from 'react-bootstrap';

const SlideCard = () => {
    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        appendDots: (dots) => {
            return <ul style={{ margin: '0px' }}>{dots}</ul>;
        },
    };
    return (
        <>
            <Slider {...settings}>
                {Sdata.map((value, index) => {
                    return (
                        <>
                            <Container className="d-flex">
                                <Row>
                                    <Col md={6} lg={6}>
                                        <div className="box d_flex top" key={index}>
                                            <div className="left">
                                                <h1>{value.title}</h1>
                                                <p>{value.desc}</p>
                                                <Button
                                                    style={{
                                                        height: '50px',
                                                        width: '180px',
                                                        backgroundColor: '#ff2665  ',
                                                        border: 'none',
                                                    }}
                                                >
                                                    Visit Collections
                                                </Button>
                                            </div>
                                        </div>
                                    </Col>
                                    <Col md={6} lg={6}>
                                        <div className="right">
                                            <img className="img-fluid" src={value.cover} alt="" />
                                        </div>
                                    </Col>
                                </Row>
                            </Container>
                        </>
                    );
                })}
            </Slider>
        </>
    );
};

export default SlideCard;
